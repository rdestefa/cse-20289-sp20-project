# Project - README

This is the [Final Project] for [CSE 20289 Systems Programming (Spring 2020)].

## Members

- Ryan DeStefano (rdestefa@nd.edu)

## Demonstration

- [Link to Demonstration Video](https://youtu.be/ztq9LfW12aE)

## Errata

Everything seems to work correctly. I ran valgrind with the test script in forking mode to see if valgrind reported any errors when each child process exited, and there were no memory issues. The code compiles without errors or warnings under all of the flags provided.

## Contributions

As I was the sole member for the project, all contributions were made by me.

[Final Project]: https://www3.nd.edu/~pbui/teaching/cse.20289.sp20/project.html
[CSE 20289 Systems Programming (Spring 2020)]: https://www3.nd.edu/~pbui/teaching/cse.20289.sp20/